package org.example.HW4_1;

public class TwoDimensionalArrays_1 {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int N = matrix.length;

        for (int[] ints : matrix) {
            for (int j = 0; j < N; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
        // Головна діагональ
        System.out.print("Головна діагональ: ");
        for (int i = 0; i < N; i++) {
            System.out.print(matrix[i][i] + ", ");
        }
        System.out.println();

        // Побічна діагональ
        System.out.print("Побічна діагональ: ");
        for (int i = N - 1; i >= 0; i--) {
            System.out.print(matrix[i][N - i - 1] + ", ");
        }
        System.out.println();
    }
}
