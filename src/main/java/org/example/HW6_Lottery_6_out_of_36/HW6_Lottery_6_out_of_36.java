package org.example.HW6_Lottery_6_out_of_36;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HW6_Lottery_6_out_of_36 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 36; i++) {
            numbers.add(i);
        }

        Random random = new Random();

        List<Integer> selectedNumbers = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            int randomNumber = random.nextInt(numbers.size());
            int selectedNumber = numbers.get(randomNumber);
            selectedNumbers.add(selectedNumber);
            numbers.remove(randomNumber);
            System.out.println("Випадкове число " + ( i + 1) + ": " + selectedNumber);
        }
    }
}

