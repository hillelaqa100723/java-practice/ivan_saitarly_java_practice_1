package org.example.HW_8_Rework_the_calculator_for_working_with_files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
public class HW_8_Rework_the_calculator_for_working_with_files {
    public enum Operation {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/");

        private final String symbol;

        Operation(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    public static void main(String[] args) {
        File dataFile = new File("files/data.txt");
        List<String> data = getDataFromFile(dataFile);

        if (data != null && data.size() == 3) {
            Double result = calculate(data);
            if (result != null) {
                String resultString = prepareResultString(data, result);
                System.out.println();
                System.out.println(resultString);
            } else {
                System.out.println("Calculation error!");
            }
        } else {
            System.out.println("Invalid data format!");
        }
    }

    public static List<String> getDataFromFile(File dataFile) {
        try {
            String content = new String(Files.readAllBytes(Paths.get(dataFile.getAbsolutePath())));
            String[] parts = content.split(", ");
            return List.of(parts);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Double calculate(List<String> data) {
        if (data.size() != 3) {
            return null;
        }

        try {
            Double operand1 = Double.parseDouble(data.get(0));
            Operation operation = Operation.valueOf(data.get(1));
            Double operand2 = Double.parseDouble(data.get(2));

            switch (operation) {
                case PLUS:
                    return operand1 + operand2;
                case MINUS:
                    return operand1 - operand2;
                case MULTIPLY:
                    return operand1 * operand2;
                case DIVIDE:
                    if (operand2 != 0) {
                        return operand1 / operand2;
                    } else {
                        System.out.println("Division by zero!");
                        return null;
                    }
                default:
                    System.out.println("Unsupported operation!");
                    return null;
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format!");
            return null;
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid operation!");
            return null;
        }
    }

    public static String prepareResultString(List<String> data, Double result) {
        String operationSymbol = data.get(1);
        return data.get(0) + " " + operationSymbol + " " + data.get(2) + " = " + result;
    }
}
