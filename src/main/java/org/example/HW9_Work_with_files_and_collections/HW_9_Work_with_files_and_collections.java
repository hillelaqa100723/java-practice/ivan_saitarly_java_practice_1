package org.example.HW9_Work_with_files_and_collections;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class HW_9_Work_with_files_and_collections {
    public static void main(String[] args) throws IOException {
        File dataFile = new File("files/data.csv");

        Map<Integer, String> mapData = getDataFromFile(dataFile);

        int idToSearch = 3522;
        String userData = getDataById(mapData, idToSearch);

        if (userData != null){
            System.out.println("User data for id " + idToSearch + ": " + userData);
        } else {
            System.out.println("User data with id " + idToSearch + " not found!");
        }

        String lastName = "Ivanov";
        int locations = getNumberOfOccurrences(mapData, lastName);
        System.out.println("Qty of locations for lastName " + lastName + ": " + locations);
    }

    public static Map<Integer, String> getDataFromFile(File dataFile) throws IOException {
        Map<Integer, String> stringMap = new HashMap<>();

        try {
            for (String line : FileUtils.readLines(dataFile, Charset.defaultCharset())){
                String [] parts = line.split(",");
                if (parts.length >= 2){
                    Integer id = Integer.parseInt(parts[0].trim());
                    String value = parts[1].trim();
                    stringMap.put(id, value);
                }

            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return stringMap;
    }
    static String getDataById(Map<Integer, String> mapData, Integer id){
        return mapData.get(id);
    }
    static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName){
        int count = 0;

        for (String fullName : mapData.values()){
            String[] nameParts = fullName.split(" ");
            if (nameParts.length >= 2){
                String lastNameFromData = nameParts[0].trim();
                if (lastNameFromData.equals(lastName)){
                    count++;
                }
            }
        }
        return count;
    }

}