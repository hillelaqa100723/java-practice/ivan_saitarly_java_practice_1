package org.example.hw5;

public class CalculationOfSumsOfArrayRows {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        System.out.println();

        int[] sums = sumsInRows(matrix);

        System.out.print("Результат: ");
        for (int sum : sums) {
            System.out.print((sum) + ", ");
        }
        System.out.println();
    }

    public static int[] sumsInRows(int[][] source) {
        int N = source.length;
        int [] sums = new int[N];

        for (int i = 0; i < N; i++) {
            int rowSum = 0;
            for (int j = 0; j < N; j++) {
                rowSum += source[i][j];
            }
            sums[i] = rowSum;
        }
            return sums;
    }
}