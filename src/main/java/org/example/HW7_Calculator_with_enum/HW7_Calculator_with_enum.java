package org.example.HW7_Calculator_with_enum;

import java.util.ArrayList;
import java.util.List;

public class HW7_Calculator_with_enum {

        public enum CalOperational {
            PLUS("+"), MINUS("-"), MULTIPLY("*"), DIVIDE("/"), REMAINDER("%");

            private final String symbol;

            CalOperational(String symbol) {
                this.symbol = symbol;
            }

            public String getSymbol() {
                return symbol;
            }
        }

        public static Double calculate(List<String> data) {
            if (data == null || data.isEmpty()) {
                System.out.println("Input data is empty or null");
                return null;
            }

            double result;
            try {
                result = Double.parseDouble(data.get(0));
            } catch (NumberFormatException e) {
                System.out.println("Invalid input format for number: " + data.get(0));
                return null;
            }

            for (int i = 1; i < data.size(); i += 2) {
                if (i + 1 >= data.size()) {
                    System.out.println("Invalid input format");
                    return null;
                }
                CalOperational operation;
                try {
                    operation = CalOperational.valueOf(data.get(i));
                } catch (IllegalArgumentException a) {
                    System.out.println("Invalid operation: " + data.get(i));
                    return null;
                }

                double number;
                try {
                    number = Double.parseDouble(data.get(i + 1));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input format for number: " + data.get(i + 1));
                    return null;
                }

                switch (operation) {
                    case PLUS:
                        result += number;
                        break;
                    case MINUS:
                        result -= number;
                        break;
                    case MULTIPLY:
                        result *= number;
                        break;
                    case DIVIDE:
                        if (number == 0) {
                            System.out.println("Division by zero");
                            return null;
                        }
                        result /= number;
                        break;
                    case REMAINDER:
                        if (number == 0) {
                            System.out.println("Remainder by zero");
                            return null;
                        }
                        result %= number;
                        break;
                }
            }
            return result;
        }

        public static String prepareResultString(List<String> data, Double result) {
            StringBuilder resultString = new StringBuilder();
            for (String item : data) {
                resultString.append(item).append(" ");
            }

            if (result != null) {
                resultString.append("= ").append(result);
            } else {
                resultString.append("= null");
            }
            return resultString.toString();
        }

        public static void main(String[] args) {
            List<String> data = new ArrayList<>();
            data.add("3");
            data.add("MINUS");
            data.add("1");
            Double result = calculate(data);
            String resultString = prepareResultString(data, result);
            System.out.println(resultString);
        }
}
