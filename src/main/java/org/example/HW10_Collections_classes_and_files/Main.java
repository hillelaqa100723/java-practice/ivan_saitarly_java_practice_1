package org.example.HW10_Collections_classes_and_files;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        File dataFile = new File("files/data1.csv");
        Map<Integer, User> userMap = getDataFromFile(dataFile);

        User user = getDataById(userMap, 1111);

        int numberOfOccurrences = getNumberOfOccurrences(userMap, "Paramonov");
        System.out.println("Number of occurrences for last name: " + numberOfOccurrences);
        System.out.println();

        int ageToCheck = 40;
        List<User> usersAboveAge = getUsersAgeMoreThan(userMap, ageToCheck);
        System.out.println("Users above age " + ageToCheck + ":");
        if (usersAboveAge.isEmpty()) {
            System.out.println("No users above age " + ageToCheck);
        } else {
            for (User u : usersAboveAge) {
                System.out.println(u.getFirstName() + " " + u.getLastName());
            }
        }

        System.out.println();

        Set<String> uniqueLastNames = new HashSet<>();
        for (User u : userMap.values()) {
            uniqueLastNames.add(u.getLastName());
        }

        for (String lastName : uniqueLastNames) {
            numberOfOccurrences = getNumberOfOccurrences(userMap, lastName);
            System.out.println("Number of users with last name " + lastName + ": " + numberOfOccurrences);
        }
    }

    public static Map<Integer, User> getDataFromFile(File dataFile) {
        Map<Integer, User> userMap = new HashMap<>();

        try {
            List<String> lines = FileUtils.readLines(dataFile, Charset.defaultCharset());
            for (String srt : lines) {
                String[] split = srt.split(",");

                Integer id = Integer.parseInt(split[0].trim());
                String lastName = split[1];
                String firstName = split[2];
                Integer age = Integer.parseInt(split[3].trim());

                User user = new User(firstName, lastName, age);
                userMap.put(id, user);

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return userMap;
    }

    public static User getDataById(Map<Integer, User> mapData, Integer id) {
        User user = mapData.get(id);
        if ( user != null){
            System.out.println();
            System.out.println("User with ID " + id + ": " + user.getFirstName() + " " + user.getLastName());
            System.out.println();
        } else {
            System.out.println();
            System.out.println("User with ID " + id + " not found.");
            System.out.println();
        }
        return user;
    }

    public static int getNumberOfOccurrences(Map<Integer, User> mapData, String lastName) {
        int count = 0;
        for (User user : mapData.values()) {
            if (user.getLastName().equals(lastName)) {
                count++;
            }
        }
        return count;
    }

    public static List<User> getUsersAgeMoreThan(Map<Integer, User> mapData, int age) {
        List<User> result = new ArrayList<>();
        for (User user : mapData.values()) {
            if (user.getAge() > age) {
                result.add(user);
            }
        }
        return result;
    }
    public static Map<String, List<Integer>> findEqualUsers(Map<Integer,User> users){
        Map<String, List<Integer>> equalUsersMap = new HashMap<>();
        Map<String, List<Integer>> tempMap = new HashMap<>();

        for (Map.Entry<Integer, User> entry : users.entrySet()){
            String nameKey = entry.getValue().getFirstName() + " " + entry.getValue().getLastName();
            if (tempMap.containsKey(nameKey)){
                tempMap.get(nameKey).add(entry.getKey());
            } else {
                List<Integer> idList = new ArrayList<>();
                idList.add(entry.getKey());
                tempMap.put(nameKey,idList);
            }
        }
        for (Map.Entry<String, List<Integer>> entry : tempMap.entrySet()){
            if (entry.getValue().size() > 1){
                equalUsersMap.put(entry.getKey(), entry.getValue());
            }
        }
        return equalUsersMap;
    }
}

