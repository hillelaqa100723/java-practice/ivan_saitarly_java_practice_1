package org.example.org.hw1;

public class Calculator {
    public static void main(String[] args) {
        int a = 20;
        int b = 7;

        printResult("Plus: ", plus(a, b));
        printResult("Minus: ",minus(a, b));
        printResult("Mult: ", mult(a, b));
        printResult("Div: ", div(a, b));
        printResult("Rem: ", rem(a, b));


    }

    public static void printResult(String comment, int value){
        System.out.println(comment + value);
    }

    /**
     * Adding ane integer to another
     * @param a first integer
     * @param b second integer
     * @return result as integer
     */

public static int plus(int a, int b){
        return a + b;
    }
    public static int minus(int a, int b){
        return a - b;
    }
    public static int mult(int a, int b){
        return a * b;
    }
    public static int div(int a, int b){
    if (b != 0) {
        return a / b;
    } else {
        System.out.println("You cannot divide on 0!");
        return  -10000;
        }
    }
    public static int rem(int a, int b){
        if (b != 0) {
            return a % b;
        } else {
            System.out.println("You cannot divide on 0!");
            return  -10000;
        }
    }
}
